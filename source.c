// gtklock-countdown-module
// Copyright (c) 2022-23 Erik Reider, Jovan Lanik, Bob Hepple

// CSS text module

#include "gtklock-module.h"

#define MODULE_DATA(x) (x->module_data[self_id])
#define COUNTDOWN(x) ((struct countdown *)MODULE_DATA(x))

extern void config_load(const char *path, const char *group, GOptionEntry entries[]);

struct countdown {
	GtkWidget *countdown_revealer;
	GtkWidget *countdown_box;
	GtkWidget *countdown_label;
};

const gchar module_name[] = "countdown";
const guint module_major_version = 4;
const guint module_minor_version = 0;

static int self_id;

static gchar *command = NULL;
static int refresh_time = 0;
static gchar *position = NULL;
static gchar *justify = NULL;
static int margin_top = 0;
static int margin_bottom = 0;
static int margin_left = 0;
static int margin_right = 0;
static int countdown = 0;
static int countdown_after = 0;

GOptionEntry module_entries[] = {
    { "countdown-position", 0, 0, G_OPTION_ARG_STRING, &position, "Position of text (top-left, bottom-center, etc)", NULL },
    { "justify", 0, 0, G_OPTION_ARG_STRING, &justify, "Justify the text (left or right)", NULL },
    { "margin-top", 0, 0, G_OPTION_ARG_INT, &margin_top, "Top margin", NULL },
    { "margin-bottom", 0, 0, G_OPTION_ARG_INT, &margin_bottom, "Bottom margin", NULL },
    { "margin-left", 0, 0, G_OPTION_ARG_INT, &margin_left, "Left margin", NULL },
    { "margin-right", 0, 0, G_OPTION_ARG_INT, &margin_right, "Right margin", NULL },
    { "countdown", 0, 0, G_OPTION_ARG_INT, &countdown, "Countdown for <secs>.", NULL },
    { "countdown-after", 0, 0, G_OPTION_ARG_INT, &countdown_after, "Start timer after <secs>.", NULL },
	{ NULL },
};

gboolean countdown_callback(gpointer data)
{
    struct Window *ctx = (struct Window *)data;
    int retval = FALSE;
    
    if (countdown > 0) {
        GString* string = g_string_new("");

        g_string_printf(string, "<span size='100000' foreground='yellow'>%d</span>", countdown--);
        gtk_label_set_markup(GTK_LABEL(COUNTDOWN(ctx)->countdown_label), string->str);
        g_string_free(string, TRUE);
        retval=TRUE;
    } else {
        gtk_label_set_markup(GTK_LABEL(COUNTDOWN(ctx)->countdown_label), "");
        retval = FALSE;
    }
    return retval;
}

// triggers on countdown_after
gboolean countdown_start_callback(gpointer data)
{
    struct Window *ctx = (struct Window *)data;

    // first count:
    countdown_callback(data);
    // subsequent counts:
    g_timeout_add_seconds(1, countdown_callback, (gpointer)ctx);

    // Return FALSE so it's only called once
    return FALSE;
}

static void setup_countdown(struct Window *ctx) {
	if(MODULE_DATA(ctx) != NULL) {
		gtk_widget_destroy(COUNTDOWN(ctx)->countdown_revealer);
		g_free(MODULE_DATA(ctx));
		MODULE_DATA(ctx) = NULL;
	}
	MODULE_DATA(ctx) = g_malloc(sizeof(struct countdown));

	COUNTDOWN(ctx)->countdown_revealer = gtk_revealer_new();
	gtk_widget_set_halign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_CENTER);
	gtk_widget_set_name(COUNTDOWN(ctx)->countdown_revealer, "countdown-revealer");
	gtk_revealer_set_reveal_child(GTK_REVEALER(COUNTDOWN(ctx)->countdown_revealer), TRUE);
	gtk_revealer_set_transition_type(GTK_REVEALER(COUNTDOWN(ctx)->countdown_revealer), GTK_REVEALER_TRANSITION_TYPE_NONE);
	gtk_overlay_add_overlay(GTK_OVERLAY(ctx->overlay), COUNTDOWN(ctx)->countdown_revealer);

    if(
        g_strcmp0(position, "top-left") == 0 ||
        g_strcmp0(position, "center-left") == 0 ||
        g_strcmp0(position, "bottom-left") == 0
        ) gtk_widget_set_halign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_START);
    else if(
        g_strcmp0(position, "top-right") == 0 ||
        g_strcmp0(position, "center-right") == 0 ||
        g_strcmp0(position, "bottom-right") == 0

        ) gtk_widget_set_halign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_END);
    else gtk_widget_set_halign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_CENTER);

    if(
        g_strcmp0(position, "top-left") == 0 ||
        g_strcmp0(position, "top-right") == 0 ||
        g_strcmp0(position, "top-center") == 0
        ) gtk_widget_set_valign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_START);
    else if(
        g_strcmp0(position, "bottom-left") == 0 ||
        g_strcmp0(position, "bottom-right") == 0 ||
        g_strcmp0(position, "bottom-center") == 0
        ) gtk_widget_set_valign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_END);
    else gtk_widget_set_valign(COUNTDOWN(ctx)->countdown_revealer, GTK_ALIGN_CENTER);

    gtk_widget_set_margin_start(COUNTDOWN(ctx)->countdown_revealer, margin_left);
    gtk_widget_set_margin_end(COUNTDOWN(ctx)->countdown_revealer, margin_right);
    gtk_widget_set_margin_top(COUNTDOWN(ctx)->countdown_revealer, margin_top);
    gtk_widget_set_margin_bottom(COUNTDOWN(ctx)->countdown_revealer, margin_bottom);
                              
	COUNTDOWN(ctx)->countdown_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_widget_set_halign(COUNTDOWN(ctx)->countdown_box, GTK_ALIGN_CENTER);
	gtk_widget_set_name(COUNTDOWN(ctx)->countdown_box, "countdown-box");
	gtk_container_add(GTK_CONTAINER(COUNTDOWN(ctx)->countdown_revealer), COUNTDOWN(ctx)->countdown_box);

	COUNTDOWN(ctx)->countdown_label = gtk_label_new(NULL);
	gtk_widget_set_name(COUNTDOWN(ctx)->countdown_label, "countdown");
	g_object_set(COUNTDOWN(ctx)->countdown_label, "margin-bottom", 10, NULL);
	gtk_container_add(GTK_CONTAINER(COUNTDOWN(ctx)->countdown_box), COUNTDOWN(ctx)->countdown_label);

    if (g_strcmp0(justify, "right") == 0) 
        gtk_label_set_justify(GTK_LABEL(COUNTDOWN(ctx)->countdown_label), GTK_JUSTIFY_RIGHT);
    else if (g_strcmp0(justify, "center") == 0) 
        gtk_label_set_justify(GTK_LABEL(COUNTDOWN(ctx)->countdown_label), GTK_JUSTIFY_CENTER);
    else if (g_strcmp0(justify, "fill") == 0) 
        gtk_label_set_justify(GTK_LABEL(COUNTDOWN(ctx)->countdown_label), GTK_JUSTIFY_FILL);

	gtk_widget_show_all(COUNTDOWN(ctx)->countdown_revealer);

    // Set up countdown
    if (countdown > 0) {
        g_timeout_add_seconds(countdown_after, countdown_start_callback, (gpointer)ctx);
    }
}

void g_module_unload(GModule *m) {
	// g_object_unref(countdown); // gives seg.viol.
    g_free(position);
    g_free(justify);
}

void on_activation(struct GtkLock *gtklock, int id) {
	self_id = id;

	GtkCssProvider *provider = gtk_css_provider_new();
	GError *err = NULL;
	const char css[] =
		"#countdown {"
		"font-size: 18pt;"
		"}"
		;

	gtk_css_provider_load_from_data(provider, css, -1, &err);
	if(err != NULL) {
		g_warning("Style loading failed: %s", err->message);
		g_error_free(err);
	} else {
		gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
			GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	}

	g_object_unref(provider);
}

void on_focus_change(struct GtkLock *gtklock, struct Window *win, struct Window *old) {
	setup_countdown(win);
	if(gtklock->hidden)
		gtk_revealer_set_reveal_child(GTK_REVEALER(COUNTDOWN(win)->countdown_revealer), FALSE);
	if(old != NULL && win != old)
		gtk_revealer_set_reveal_child(GTK_REVEALER(COUNTDOWN(old)->countdown_revealer), FALSE);
}

void on_window_destroy(struct GtkLock *gtklock, struct Window *ctx) {
	if(MODULE_DATA(ctx) != NULL) {
		g_free(MODULE_DATA(ctx));
		MODULE_DATA(ctx) = NULL;
	}
}

void on_idle_hide(struct GtkLock *gtklock) {
	if(gtklock->focused_window) {
		GtkRevealer *revealer = GTK_REVEALER(COUNTDOWN(gtklock->focused_window)->countdown_revealer);	
		gtk_revealer_set_reveal_child(revealer, FALSE);
	}
}

void on_idle_show(struct GtkLock *gtklock) {
	if(gtklock->focused_window) {
		GtkRevealer *revealer = GTK_REVEALER(COUNTDOWN(gtklock->focused_window)->countdown_revealer);	
		gtk_revealer_set_reveal_child(revealer, TRUE);
	}
}


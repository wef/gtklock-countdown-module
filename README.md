# gtklock-countdown-module
[gtklock](https://github.com/jovanlanik/gtklock) module to do a countdown on a lock-screen.
##
![image](screenshot1.png)
## About
[gtklock](https://github.com/jovanlanik/gtklock) module to do a countdown on a lock-screen.

xbps-packages template is at https://gitlab.com/wef/void-templates/-/tree/main/gtklock-countdown-module

*** WARNING: ALPHA STATUS. BUT IT WORKS FOR ME. ***

### config file
`countdown-position=`[top|center|bottom]-[left|center|right]

`justify=`[left|right|center|fill]

`margin-[top|bottom|left|right]=`\<pixels>

`countdown-after=`\<secs> start a countdown after <secs>

`countdown=`\<secs> number of seconds to countdown.

### config file examples
```
[countdown]
countdown-position=top-right
justify=right
margin-right=10
countdown=20
countdown-after=350
```
### Sample CSS
To control the appearance of the countdown field, you can
put this in the CSS file with `gtklock --style`:
```
#countdown {
    color: white;
    text-shadow: 1px 1px 2px black;
}
```
##
Based on code from Erik Reider's [gtklock fork](https://github.com/ErikReider/gtklock/tree/user-picture-name).
The `gtklock-module.h` header can be used when making your own modules.

__⚠️ Module version matches the compatible gtklock version. Other versions might or might not work.__
## Dependencies
- GNU Make (build-time)
- pkg-config (build-time)
- gtk+3.0

<!-- 
Local Variables:
mode: gfm
markdown-command: "pandoc -s -f gfm --metadata title=README"
eval: (add-hook 'after-save-hook (lambda nil (shell-command (concat markdown-command " README.md > README.html"))) nil 'local)
End:
-->
